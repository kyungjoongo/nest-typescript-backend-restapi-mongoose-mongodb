export class CreateBoardDto {
    name: String;
    content: String;
    title: String;
    date: String;
}
