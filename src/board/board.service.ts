import {Model} from 'mongoose';
import {Get, Injectable} from '@nestjs/common';
import {InjectModel} from '@nestjs/mongoose';
import {Board} from './board.interface';
import {CreateBoardDto} from './create-board.dto';

@Injectable()
export class BoardService {
    constructor(@InjectModel('Board') private readonly boardModel: Model<Board>) {
    }

    async create(dto: CreateBoardDto): Promise<Board> {

        let date = new Date().toLocaleDateString('ko-KR')
        let time = new Date().toLocaleTimeString('ko-KR')
        dto.date = date.toString() + time.toString();
        const createdCat = new this.boardModel(dto);
        return await createdCat.save();
    }

    async findAll(): Promise<Board[]> {
        let __results: any = await this.boardModel.find().sort([['date'.toString(), 'descending']]).exec();

        return __results;
    }
    async fetchAllimages(): Promise<Board[]> {
        let __results: any = await this.boardModel.find().sort([['date'.toString(), 'descending']]).exec();
        let newList = [];
        for (let i in __results) {

            console.log('debug', __results[i].image);

            if ( __results[i].image!=undefined){
                newList.push(
                    __results[i]
                );
            }


        }
        console.log('__results', newList);
        return newList;
    }


    async findOne(id: string): Promise<Board> {
        let resultOne = await this.boardModel.findById(id).then((doc: any) => {

            console.log('debug', doc._doc);

            return doc._doc;
        });

        console.log('__results', resultOne);
        return resultOne;

    }

    async updateOne(dto: CreateBoardDto, id: string): Promise<Board> {
        let resultOne = await this.boardModel.update({_id: id}, dto, {upsert: true});
        console.log('__results', resultOne);
        return resultOne;

    }

    async deleteOne(id: string): Promise<any> {
        let resultOne = await this.boardModel.deleteOne({_id: id});
        console.log('__results', resultOne);
        return resultOne;

    }
}

