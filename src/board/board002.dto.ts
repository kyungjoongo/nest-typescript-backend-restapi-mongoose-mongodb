export class Board002Dto {
    name: string;
    content: string;
    title: string;
    date: string;
    image: string;
}
