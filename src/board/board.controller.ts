import {Controller, Get, Post, Body, Param, Put, Delete, Response, Render} from '@nestjs/common';
import {BoardService} from './board.service';
import {CreateBoardDto} from './create-board.dto';
import {Board} from './board.interface';
import * as express from 'express';

@Controller('board')
export class BoardController {
    constructor(private readonly boardService: BoardService) {
    }

    @Post()
    async create(@Body() createCatDto: CreateBoardDto) {
        await this.boardService.create(createCatDto);
    }

    @Get()
    async findAll(): Promise<Board[]> {
        return this.boardService.findAll();
    }

    @Get('/images')
    async images(): Promise<Board[]> {
        return this.boardService.fetchAllimages();
    }

    @Get(':id')
    async findOne(@Param() params): Promise<Board> {

        console.log('id====>', params.id);
        let _id = params.id;
        return this.boardService.findOne(_id);
    }

    @Put(':id')
    async update(@Param('id') id: string, @Body() dto: CreateBoardDto) {

        console.log('debug', dto);
        await this.boardService.updateOne(dto, id);
    }

    @Delete(':id')
    async remove(@Param('id') id: string) {
        console.log('debug', id);
        await this.boardService.deleteOne(id);
    }

    @Get('/delete/:id')
    async deleteOne(@Param() params, @Response() response: express.Response) {

        console.log('id====>', params.id);
        let _id = params.id;
        let result=await this.boardService.deleteOne(_id);

        console.log('deleteOne===>', result);
        return response.redirect(303, `./../../index`);

    }



}
