import {Module} from '@nestjs/common';
import {getModelToken, MongooseModule} from '@nestjs/mongoose';
import {BoardController} from './board.controller';
import {BoardService} from './board.service';
import {BoardSchema} from './board.schema';

@Module({
    imports: [MongooseModule.forFeature([{name: 'Board', schema: BoardSchema}])],
    controllers: [BoardController],
    providers: [BoardService],
})
export class BoardModule {
}
