import * as mongoose from 'mongoose';

export const BoardSchema = new mongoose.Schema({
    name: String,
    content: String,
    title: String,
    date: String,
    image: String,
});
