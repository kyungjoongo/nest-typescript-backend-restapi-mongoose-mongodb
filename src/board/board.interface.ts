import {Document} from 'mongoose';

export interface Board extends Document {
    name: string;
    content: string,
    title: string;
    date: string,
}
