import {Module} from '@nestjs/common';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import {MongooseModule} from '@nestjs/mongoose';
import {CatsModule} from './cats/cats.module';
import {CatsService} from './cats/cats.service';
import {CatSchema} from './cats/cat.schema';
import {BoardModule} from './board/board.module';
import {BoardSchema} from './board/board.schema';
import {BoardService} from './board/board.service';
import {FileuploadController} from './fileupload/fileupload.controller';
import {ProverbModule} from './proverb/proverb.module';
import {ProverbService} from './proverb/proverb.service';
import {ProverbSchema} from './proverb/proverb.schema';

@Module({
    imports: [
        MongooseModule.forRoot('mongodb://localhost:27017/nest'),
        CatsModule,
        BoardModule,
        ProverbModule,
        MongooseModule.forFeature([{name: 'Cat', schema: CatSchema}]),
        MongooseModule.forFeature([{name: 'Proverb', schema: ProverbSchema}]),
        MongooseModule.forFeature([{name: 'Board', schema: BoardSchema}])

    ],
    controllers: [AppController, FileuploadController],
    providers: [AppService, CatsService, BoardService, ProverbService],
})
export class AppModule {
}
