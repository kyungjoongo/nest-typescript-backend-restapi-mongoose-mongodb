import {Controller, Get, Param, Render} from '@nestjs/common';
import {AppService} from './app.service';
import {CatsService} from './cats/cats.service';
import {BoardService} from './board/board.service';

@Controller()
export class AppController {
    constructor(private readonly appService: AppService, public catsService: CatsService, public boardService: BoardService) {
    }

    /* @Get()
     getHello(): string {
         return this.appService.printKyungjoongo();
     }
 */

    @Get('/index')
    @Render('index')
    async index002() {

        let results = await this.boardService.findAll();

        return {results: results};
    }

    @Get('/')
    @Render('index')
    async root() {

        let results = await this.boardService.findAll();

        return {
            results: results,
            rlength: results.length,
        };
    }

    @Get('/imageList')
    @Render('imageList')
    async imageList() {

        let results = await this.boardService.findAll();

        return {
            results: results,
            rlength: results.length,
        };
    }


    @Get('/index002')
    @Render('index002')
    async index002_1() {

        let results = await this.boardService.findAll();

        return {
            results: results,
            rlength: results.length,
        };
    }

    @Get('/detail/:id')
    @Render('detail')
    async detail(@Param() params) {
        let _id = params.id;
        let resultOne = await this.boardService.findOne(_id);
        return {result: resultOne};
    }

    @Get('/test')
    test() {

        let arrayList = [
            {
                id: 'dslfk',
                name: 'dslfksdlkf',
                content: '고경준천잰.ㅁ.',
            },
            {
                id: 'dslfk',
                name: 'dslfksdlkf',
                content: '고경준천잰.ㅁ.',
            },
            {
                id: 'dslfk',
                name: 'dslfksdlkf',
                content: '고경준천잰.ㅁ.',
            },
            {
                id: 'dslfk',
                name: 'dslfksdlkf',
                content: '고경준천잰.ㅁ.',
            },

        ];

        return arrayList;
    }
}
