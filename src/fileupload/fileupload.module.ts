import {Module} from '@nestjs/common';
import {getModelToken, MongooseModule} from '@nestjs/mongoose';
import {FileuploadController} from './fileupload.controller';

@Module({
    imports: [],
    controllers: [FileuploadController],
    providers: [],
})
export class CatsModule {
}
