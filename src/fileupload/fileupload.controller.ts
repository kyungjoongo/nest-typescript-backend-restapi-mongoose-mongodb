import {Body, Controller, Get, Post, Render, Response, UploadedFile, UseInterceptors} from '@nestjs/common';
import {FileInterceptor} from '@nestjs/platform-express';
import {BoardService} from '../board/board.service';
import {Board002Dto} from '../board/board002.dto';
import * as express from 'express';

@Controller()
export class FileuploadController {

    constructor(public boardService: BoardService) {

    }

    @Post('/fileUploadProcess')
    @Render('/file/imageList')
    @UseInterceptors(FileInterceptor('file', {
        dest: 'public/assets/'
    }))
    async uploadFile(@UploadedFile() file, @Body() dto: Board002Dto, @Response() response: express.Response) {

        console.log('debug', dto);

        let oldFilePath = '';
        if (file === undefined) {
            file = {};
            file.filepath = '';
        } else {
            oldFilePath = file.path;
            oldFilePath = oldFilePath.replace('public/', '');
            dto.image = oldFilePath;
        }

        await this.boardService.create(dto);

        console.log('debug', oldFilePath);

        return response.redirect(303, `./index`);

    }

    @Get('/file/uploadForm')
    @Render('file/uploadForm')
    async root() {

        return {results: ''};
    }

    /*@Get('/imageList')
    @Render('index002')
    async imageList() {

        return {results: 'test'};
    }*/

}
