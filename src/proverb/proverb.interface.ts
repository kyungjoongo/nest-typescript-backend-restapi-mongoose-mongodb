import {Document} from 'mongoose';

export interface Proverb extends Document {
    name: string;
    content: string;
    date: string;
}
