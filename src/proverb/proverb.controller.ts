import {Body, Controller, Delete, Get, Param, Post, Put, Render, Response, UploadedFile, UseInterceptors} from '@nestjs/common';
import {ProverbService} from './proverb.service';
import {ProverbDto} from './proverb.dto';
import {Proverb} from './proverb.interface';
import {FileInterceptor} from '@nestjs/platform-express';
import {Board002Dto} from '../board/board002.dto';
import * as express from 'express';

@Controller('proverb')
export class ProverbController {
    constructor(private readonly proverbService: ProverbService) {
    }

    @Post()
    async create(@Body() createCatDto: ProverbDto) {
        await this.proverbService.create(createCatDto);
    }

    @Get()
    async findAll(): Promise<Proverb[]> {
        return this.proverbService.findAll();
    }

    @Render('proverb/index')
    @Get('/index')
    async index(): Promise<Proverb[]> {
        return this.proverbService.findAll();
    }


    @Post('/fileUploadProcess')
    @Render('/file/imageList')
    @UseInterceptors(FileInterceptor('file', {
        dest: 'public/assets/'
    }))
    async uploadFile(@UploadedFile() file, @Body() dto: Board002Dto, @Response() response: express.Response) {

        console.log('debug', dto);

        let oldFilePath = '';
        if (file === undefined) {
            file = {};
            file.filepath = '';
        } else {
            oldFilePath = file.path;
            oldFilePath = oldFilePath.replace('public/', '');
            dto.image = oldFilePath;
        }

        await this.proverbService.create(dto);

        console.log('debug', oldFilePath);

        return response.redirect(303, `./`);

    }

    @Get('/images')
    async findAllImages(): Promise<Proverb[]> {

        let result: any = await this.proverbService.findAll();

        for ( let i in result){
            console.log('debug', result[i].image);
        }

        console.log('debug', result);

        return result;
    }

    @Get(':id')
    async findOne(@Param() params): Promise<Proverb> {

        console.log('id====>', params.id);
        let _id = params.id;
        return this.proverbService.findOne(_id);
    }

    @Put(':id')
    async update(@Param('id') id: string, @Body() dto: ProverbDto) {

        console.log('debug', dto);
        await this.proverbService.updateOne(dto, id);
    }

    @Delete(':id')
    async remove(@Param('id') id: string) {
        console.log('debug', id);
        await this.proverbService.deleteOne(id);
    }

}
