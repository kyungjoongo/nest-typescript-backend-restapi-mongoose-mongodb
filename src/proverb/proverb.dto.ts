export class ProverbDto {
    name: string;
    content: string;
    date: string;
}
