import {Model} from 'mongoose';
import {Injectable} from '@nestjs/common';
import {InjectModel} from '@nestjs/mongoose';
import {Proverb} from './proverb.interface';
import {ProverbDto} from './proverb.dto';

@Injectable()
export class ProverbService {
    constructor(@InjectModel('Proverb') private  proverbModel: Model<Proverb>) {
    }

    async create(dto: ProverbDto): Promise<Proverb> {
        dto.date = new Date().toUTCString();
        const createdCat = new this.proverbModel(dto);
        return await createdCat.save();
    }

    async findAll(): Promise<Proverb[]> {
        let __results = await this.proverbModel.find().sort([['date'.toString(), 'descending']]).exec();

        console.log('__results', __results);
        return __results;
    }

    async findOne(id: string): Promise<Proverb> {
        let resultOne = await this.proverbModel.findById(id);

        console.log('__results', resultOne);
        return resultOne;

    }

    async updateOne(dto: ProverbDto, id: string): Promise<Proverb> {
        let resultOne = await this.proverbModel.update({_id: id}, dto, {upsert: true});
        console.log('__results', resultOne);
        return resultOne;

    }

    async deleteOne(id: string): Promise<any> {
        let resultOne = await this.proverbModel.deleteOne({_id: id});
        console.log('__results', resultOne);
        return resultOne;

    }
}

