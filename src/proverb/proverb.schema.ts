import * as mongoose from 'mongoose';

export const ProverbSchema = new mongoose.Schema({
    name: String,
    content: String,
    date: String,
});
