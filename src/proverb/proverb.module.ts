import {Module} from '@nestjs/common';
import {MongooseModule} from '@nestjs/mongoose';
import {ProverbController} from './proverb.controller';
import {ProverbService} from './proverb.service';
import {ProverbSchema} from './proverb.schema';

@Module({
    imports: [MongooseModule.forFeature([{name: 'Proverb', schema: ProverbSchema}])],
    controllers: [ProverbController],
    providers: [ProverbService],
})
export class ProverbModule {
}
