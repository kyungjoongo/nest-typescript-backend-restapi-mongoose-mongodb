import {Controller, Get, Post, Body, Param, Put, Delete} from '@nestjs/common';
import {CreateCatDto} from './create-cat.dto';
import {CatsService} from './cats.service';
import {Cat} from './cat.interface';
import {UpdateCatDto} from './update-cat.dto';

@Controller('cats')
export class CatsController {
    constructor(private readonly catsService: CatsService) {
    }

    @Post()
    async create(@Body() createCatDto: CreateCatDto) {

        await this.catsService.create(createCatDto);
    }

    @Get()
    async findAll(): Promise<Cat[]> {
        return this.catsService.findAll();
    }

    @Get(':id')
    async findOne(@Param() params): Promise<Cat> {

        console.log('id====>', params.id);
        let _id = params.id;
        return this.catsService.findOne(_id);
    }

    @Put(':id')
    async update(@Param('id') id: string, @Body() updateCatDto: UpdateCatDto) {

        console.log('debug', updateCatDto);
        await this.catsService.updateOne(updateCatDto, id);
    }

    @Delete(':id')
    async remove(@Param('id') id: string) {
        console.log('debug', id);
        await this.catsService.deleteOne(id);
    }
}
