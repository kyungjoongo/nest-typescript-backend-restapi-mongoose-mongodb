import {Model} from 'mongoose';
import {Injectable} from '@nestjs/common';
import {InjectModel} from '@nestjs/mongoose';
import {Cat} from './cat.interface';
import {CreateCatDto} from './create-cat.dto';
import {UpdateCatDto} from './update-cat.dto';

@Injectable()
export class CatsService {
    constructor(@InjectModel('Cat') private readonly catModel: Model<Cat>) {
    }

    async create(createCatDto: CreateCatDto): Promise<Cat> {

        createCatDto.date = new Date().toTimeString();
        const createdCat = new this.catModel(createCatDto);
        return await createdCat.save();
    }

    async findAll(): Promise<Cat[]> {

        let __results = await this.catModel.find().sort([['date'.toString(), 'descending']]).exec();

        console.log('__results', __results);
        return __results;
    }

    async findOne(id: string): Promise<Cat> {
        let resultOne = await this.catModel.findById(id);

        console.log('__results', resultOne);
        return resultOne;

    }

    async updateOne(updateCatDto: UpdateCatDto, id: string): Promise<Cat> {
        let resultOne = await this.catModel.update({_id: id}, updateCatDto, {upsert: true});
        console.log('__results', resultOne);
        return resultOne;

    }

    async deleteOne(id: string): Promise<any> {
        let resultOne = await this.catModel.deleteOne({_id: id});
        console.log('__results', resultOne);
        return resultOne;

    }
}

